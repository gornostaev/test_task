from __future__ import annotations

from pydantic import Field
from pydantic_settings import BaseSettings, SettingsConfigDict


class DbConfig(BaseSettings):
    DB_URL: str = Field(alias="DB_URL", default='')
    DB_NAME: str = Field(alias="DB_NAME", default='')
    
    @property
    def HOST(s):
        return f"somedb://{s.DB_URL}/{s.DB_NAME}"


class Config(BaseSettings):
    db: DbConfig = DbConfig()

    model_config = SettingsConfigDict(extra='ignore')


