from abc import ABC, abstractmethod
from injector import inject, Module, Binder, ClassProvider

from app.infrastructure.database import DatabaseManager
from app.config import Config
from .domain import Bet, EventResult, EventUpdate, BetRejectedError, EventUpdateError


class DatabaseRepository(ABC):
    @inject
    def __init__(self, db: DatabaseManager, config: Config):
        self.db = db
        self.config = config


class BetsRepository(DatabaseRepository):
    @abstractmethod
    async def add_bet(self, bet: Bet):
        pass

    @abstractmethod
    async def update(self, event_update: EventUpdate):
        pass

    @abstractmethod
    async def get_bets(self) -> list[Bet]:
        pass


class MockqlaBetsRepository(BetsRepository):
    async def add_bet(self, bet: Bet):
        if await self._is_event_finished(bet.event_id):
            raise BetRejectedError('Event is finished')
        if bet.amount <= 0:
            raise BetRejectedError('Amount have to be >0')

        await self.db.insert(query=bet.event_id, data=bet.model_dump(exclude='event_id', mode="json"))

    async def update(self, event_update: EventUpdate):
        if await self._is_event_finished(event_update.event_id):
            raise EventUpdateError('Event was updated before')

        await self.db.update(query=event_update.event_id, data={'result': event_update.result})

    async def get_bets(self) -> list[Bet]:
        result = []
        bets = await self.db.get_all()

        for event_id, bets in bets.items():
            for b in bets:
                result.append(Bet(event_id=event_id, **b))
        return result

    async def _is_event_finished(self, event_id):
        bets = await self.db.get_all()
        if event_id not in bets or bets[event_id][0]['result'] == EventResult.NOT_PLAYED_YET.value:
            return False

        return True


class RepositoryModule(Module):
    def configure(self, binder: Binder) -> None:
        binder.bind(BetsRepository, to=ClassProvider(MockqlaBetsRepository))