from injector import inject
from .repository import BetsRepository
from .domain import Bet, EventUpdate


class BettingUsecase:
    @inject
    def __init__(self, repository: BetsRepository):
        self.repository = repository

    async def create_new_bet(self, event_id, amount):
        await self.repository.add_bet(
            Bet(
                event_id=event_id,
                amount=amount
            )
        )

    async def finish_event(self, event: EventUpdate):
       await self.repository.update(event)

    async def get_all_bets(self):
        return await self.repository.get_bets()