from pydantic import BaseModel
from enum import Enum


class BaseCommand(BaseModel):
    pass


class BetRejectedError(Exception):
    pass


class EventUpdateError(Exception):
    pass


class EventResult(Enum):
    WIN = "WIN"
    LOOSE = "LOOSE"
    NOT_PLAYED_YET = "NOT_PLAYED_YET"


class Bet(BaseCommand):
    event_id: str
    amount: int # in cents
    result: EventResult = EventResult.NOT_PLAYED_YET


class EventUpdate(BaseCommand):
    event_id: str
    result: EventResult