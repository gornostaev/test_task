from __future__ import annotations

import os

import injector
from injector import ClassProvider, singleton, Injector

from .config import Config
from .infrastructure.database import DatabaseManager, MockDB
from .core.repository import RepositoryModule


def setup_injector(environment=None):
    if not environment:
        environment = os.getenv("ENVIRONMENT", "production")
    get_config = Config

    def configure(binder: injector.Binder):
        binder.bind(Config, to=ClassProvider(get_config))
        binder.bind(DatabaseManager, to=MockDB, scope=singleton)

    return Injector([
        configure,
        RepositoryModule
    ])
