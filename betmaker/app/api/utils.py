from fastapi import FastAPI
from starlette.requests import Request

from app import setup_injector

from typing import TypeVar

T = TypeVar("T")


def create_app() -> FastAPI:
    injector = setup_injector()
    fastapi_app = FastAPI(
        title="Betmaker API",
        description="Description",
        summary="Summary",
        version="0.0.1",
        contact={
            "name": "Ivan Gornostaev",
            "email": "gornostaevdev@gmail.com"
        },
        docs_url="/docs",
        redoc_url="/redoc",
    )
    fastapi_app.state.injector = injector

    return fastapi_app


class On:
    def __init__(self, dependency: type[T]):
        self._dependency = dependency

    def __call__(self, request: Request) -> T:
        response: T = request.app.state.injector.get(self._dependency)
        return response

