from app.api.utils import create_app, On
from app.core.domain import Bet, EventUpdate, EventResult, EventUpdateError, BetRejectedError
from app.core.usecase import BettingUsecase

from fastapi import Depends, HTTPException, status


app = create_app()


@app.get('/bets')
async def get_bets(uc: BettingUsecase = Depends(On(BettingUsecase))) -> list[Bet]:
    return await uc.get_all_bets()


@app.post('/bets', status_code=status.HTTP_200_OK)
async def make_bet(
        event_id: str,
        amount: int,
        uc: BettingUsecase = Depends(On(BettingUsecase))
) -> None:
    try:
        await uc.create_new_bet(event_id, amount)
    except BetRejectedError as e:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(e))


@app.put('/events/{event_id}', status_code=status.HTTP_200_OK)
async def update_event(
        event_id: str,
        result: EventResult,
        uc: BettingUsecase = Depends(On(BettingUsecase))
) -> None:
    try:
        await uc.finish_event(event=EventUpdate(
            event_id=event_id,
            result=result
        ))
    except EventUpdateError as e:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN, detail=str(e))
