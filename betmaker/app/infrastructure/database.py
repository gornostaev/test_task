from abc import abstractmethod, abstractproperty

import injector
import redis

from ..config import Config


class DatabaseManager:
    @abstractmethod
    def __init__(self):
        self.connect()

    @abstractproperty
    def client(self):
        pass

    @abstractproperty
    def db(self):
        pass

    @abstractmethod
    def connect(self, host: str, db_name: str):
        pass

    @abstractmethod
    def insert(self, query, data):
        pass

    @abstractmethod
    def update(self, query, data):
        pass

    @abstractmethod
    def get_all(self):
        pass


class MockDB(DatabaseManager):
    def __init__(self):
        self._db = {}

    async def insert(self, query, data):
        if query not in self._db:
            self._db.update({query: []})
        self._db[query].append(data)

    async def update(self, query, data):
        if query in self._db:
            for idx in range(len(self._db[query])):
                self._db[query][idx].update(data)

    async def get_all(self) -> dict:
        return self._db


class RedisDB(DatabaseManager):
    @injector.inject
    def __init__(self, config: Config):
        self.config = config

    def connect(self, host: str, db_name: str):
        self.db = redis.Redis(host=self.config.db.HOST)

