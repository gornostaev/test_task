import sys
from sqlalchemy import (create_engine, func, Column, Integer, String, Float, DateTime, ForeignKey)
from sqlalchemy.orm import DeclarativeMeta, declarative_base, sessionmaker, Session
Base: DeclarativeMeta = declarative_base()

engine = create_engine('postgresql://username:password@host:port/database_name')

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    gender = Column(String) # Enum?
    age = Column(Integer) # definitely not String

class HeartRate(Base):
    __tablename__ = 'heart_rates'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'), index=True)
    timestamp = Column(DateTime)
    heart_rate = Column(Float) # arent Integer? or you look deeper into hrv area?

def migrate(): Base.metadata.create_all(engine)

def create_session() -> Session: return sessionmaker(engine)()

class UserFilter:
    def __init__(self, session: Session):
        self._session = session

    def query_users(self, min_age, gender, min_avg_heart_rate, date_from, date_to) -> list[User]:
        '''
        # Напишите здесь запрос, который возвращает всех пользователей, которые старше'min_age' и
        # имеют средний пульс выше, чем 'min_avg_heart_rate', на определенном промежутке времени
            # min_age: минимальный возраст пользователей
            # gender: пол пользователей
            # min_avg_heart_rate: минимальный средний пульс
            # date_from: начало временного промежутка
            # date_to: конец временного промежутка

        uncomment line :58
        if you want to aggregate :min_avg_heart_rate: only in selected group of :min_age: and :gender:
        otherwise we will compare this value to all users
        '''
        aggregate_only_for_min_age_and_gender = self._session.query(
            User.id
        ).filter(
            User.age > min_age,
            User.gender == gender,
        ).subquery()

        avg_hr_aggregation = self._session.query(
            HeartRate.user_id,
            func.avg(HeartRate.heart_rate).label('avg_hr')
        ).filter(
            HeartRate.timestamp.between(date_from, date_to),
            # HeartRate.user_id.in_(aggregate_only_for_min_age_and_gender)
        ).group_by(
            HeartRate.user_id
        ).having(
            func.avg(HeartRate.heart_rate) > min_avg_heart_rate
        ).subquery()

        return self._session.query(User).filter(
            User.age > min_age,
            User.gender == gender,
        ).join(
            avg_hr_aggregation, User.id == avg_hr_aggregation.c.user_id
        ).all()

    def query_for_user(self, user_id, date_from, date_to) -> list[float]:
        '''
        # Напишите здесь запрос, который возвращает топ 10 самых высоких средних показателей 'heart_rate'
        # за часовые промежутки в указанном периоде 'date_from' и 'date_to'
            # user_id: ID пользователя
            # date_from: начало временного промежутка
            # date_to: конец временного промежутка
        '''
        return self._session.query(
            func.avg(HeartRate.heart_rate).label('avg_hr')
        ).filter(
            HeartRate.user_id == user_id,
            HeartRate.timestamp.between(date_from, date_to)
        ).group_by(
            func.date_trunc('hour', HeartRate.timestamp)
        ).order_by(
            func.avg(HeartRate.heart_rate).desc()
        ).limit(10).all()

def main():
    if len(sys.argv) != 2 or sys.argv[1] not in ('migrate', 'run_queries'):
        print("""Usage: 
        python main.py migrate
        python main.py run_queries
        """)
        return
    if sys.argv[1] == 'migrate':
        migrate()
    if sys.argv[1] == 'run_queries':
        with create_session() as session:
            repo = UserFilter(session=session)
            print(repo.query_users(), repo.query_for_user())
    return

if __name__ == "__main__":
    # sys.argv.append('run_queries')
    main()

