code review
- [simple flask app code review](https://gitlab.com/gornostaev/test_task/-/merge_requests/7)
- [fastapi strawberry core review](https://gitlab.com/gornostaev/test_task/-/merge_requests/8)

code example
- [factory example](https://gitlab.com/gornostaev/test_task/-/blob/develop/image_factory/main.py)
- [clean architecture](https://gitlab.com/gornostaev/test_task/-/blob/develop/telegram_bot)
