from abc import ABC, abstractmethod
from entities import User

class UserRepository(ABC):
    ''' abstract user repository '''
    @abstractmethod
    def get_user(self, user_id) -> User:
        pass

    @abstractmethod
    def create_user(self, user) -> User:
        pass

    @abstractmethod
    def update_user_stats(self, user_id) -> None:
        pass

    @abstractmethod
    def about_service(self):
        pass