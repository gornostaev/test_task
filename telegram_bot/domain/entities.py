import datetime

class User:
    first_name: str
    last_name: str
    username: str
    telegram_id: str
    visits: int
    created_at: datetime.datetime
    last_seen: datetime.datetime