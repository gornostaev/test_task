from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import ContextTypes
from ....domain.repository import UserRepository

async def start_command(repo: UserRepository, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    user = await repo.get_user(update.effective_user.id)

    welcome_message = f"Привет, {update.effective_user.first_name}!"
    keyboard = [
        [
            InlineKeyboardButton("О сервисе", callback_data="3"),
        ],
        [InlineKeyboardButton("Погода в Москве", callback_data="4")],
    ]

    if user:
        welcome_message += "\n\nРады видеть тебя снова!"
        keyboard[0].insert(0,
                           InlineKeyboardButton("Моя статистика", callback_data="2"))

        await repo.update_user_stats(user.telegram_id)
    else:
        welcome_message += "\n\nТы еще не зарегистрирован, давай сделаем тебе аккаунт"
        keyboard[0].insert(0,
                           InlineKeyboardButton("Зарегистрироваться", callback_data="1"))

    reply_markup = InlineKeyboardMarkup(keyboard)

    await update.message.reply_text(welcome_message, reply_markup=reply_markup)