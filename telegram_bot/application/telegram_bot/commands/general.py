from telegram.ext import Application
async def post_init_command(application: Application) -> None:
    await application.bot.set_my_commands([('start', 'Starts the bot')])