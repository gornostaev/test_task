async def button(repo, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    query = update.callback_query
    await query.answer()

    if query.data == "1":
        await repo.create_user(query.from_user)
        await query.message.reply_text("Отлично, зарегистрировались")


    elif query.data == "2":
        user = await repo.get_user(query.from_user.id)
        await query.message.reply_text(f"""{user['first_name']} {user['last_name']}
@{user['username']} зарегистрирован {user['created_at'].strftime('%d %b %Y')},
Нажимал /start {user['visits']} раз, последний раз {user['last_seen'].strftime('%d %b %Y')}""")


    elif query.data == "3":
        stats = await repo.about_service()
        await query.message.reply_text(f"""Всего пользователей: {stats['total_users']}
Первый пользователь зарегистрировался: {stats['first_user_registered'].strftime('%d %b %Y')}
Последний раз заходили: {stats['last_user_seen'].strftime('%d %b %Y')}""")


    elif query.data == "4":
        # import requests, parse yandex weather etc
        # or just:
        await query.message.reply_text("В Москве +15, солнечно ☀️")
    else:
        await query.message.reply_text("unknown button")