"""
Simple telegram bot

TODO:
1. Регистрировать новых пользователей
2. Выводить справочную информацию
3. Статистику
"""
from functools import partial
from telegram import Update
from telegram.ext import CallbackQueryHandler, ApplicationBuilder, CommandHandler

from application.telegram_bot.commands.button_menu import button
from application.telegram_bot.commands.general import post_init_command
from application.telegram_bot.commands.start import start_command
from domain.repository import UserRepository
from utils.utils import Env

from infrastructure import injector

class SimpleBot:
    @injector.inject
    def __init__(self,
                 user_repository: UserRepository,
                 api_token: str = Env("TELEGRAM_API_KEY")):
        self._user_repository = user_repository

        self._app = (ApplicationBuilder()
                     .token(api_token)
                     .post_init(post_init_command)
                     .build())
        self._app.add_handler(CommandHandler("start", partial(start_command, self._user_repository)))
        self._app.add_handler(CallbackQueryHandler(partial(button, self._user_repository)))


    def run(self):
        try:
            self._app.run_polling(allowed_updates=Update.ALL_TYPES)
        finally:
            pass


if __name__ == "__main__":
    bot = SimpleBot()
    bot.run()
