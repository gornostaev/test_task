import functools
import types
import typing
from dataclasses import dataclass


class Injector:
    """ Quick and dirty di """
    def __init__(self):
        self._container = {}

    @dataclass
    class Dependency:
        value: typing.Any
        is_singleton: bool
        def get(self, *args, **kwargs):
            return self.value if self.is_singleton else self.value()

    @functools.wraps
    def inject(self, func: types.FunctionType):
        """ Decorate functions with @inject to inject into annotated args
        Note: all args has to be annotated and persist in di-container
        """
        defaults = []
        for k, v in func.__annotations__.items():
            if v.__name__ in self._container:
                defaults.append(self._container[v.__name__].get())
            else:
                raise Exception(f'[{v.__name__}] Not in di-container')
        func.__defaults__ = tuple(defaults)
        return func

    def bind(self, A, to, singleton=False):
        """ Pass bindings to a container """
        d = self.Dependency(
            value=A() if singleton else A,
            is_singleton=singleton
        )
        self._container[to.__name__] = d

    def get(self, dependency):
        """ Get from container """
        if dependency.__name__ not in self._container:
            raise NotImplementedError(f'Dependency {dependency.__name__} is missing')
        return self._container[dependency.__name__].get()
