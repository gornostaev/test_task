''' di example
usage:
injector.bind(from, to, singleton=bool)
@inject
injector.get(to)

'''
import os


class Env:
    ''' Env descriptor '''
    def __init__(self, name=None):
        self.name = name

    def __get__(self, instance):
        if self.name is None:
            raise RuntimeError("Environment descriptor must have a name")
        try:
            value = os.environ[self.name]
        except KeyError:
            raise KeyError(f'Please, set configuration variable "{self.name}" in .env file')
        return value

    def __set_name__(self, owner, name):
        if self.name is None:
            self.name = name

    def __str__(self):
        return (
            f"Env(name={self.name})"
        )


