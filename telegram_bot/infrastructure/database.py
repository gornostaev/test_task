import motor.motor_asyncio
from utils.utils import Env
class MongoDbConnection:
    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, db_url: str = Env("MONGODB_URL"),
                 database: str = Env("MONGODB_DATABASE"), ):
        self.connection = motor.motor_asyncio.AsyncIOMotorClient(db_url)
        self.db = self.connection[database]
        return self.db

    def __enter__(self):
        return self.db

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.close()
        return