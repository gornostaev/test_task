from utils.di import Injector
from domain.repository import UserRepository
from .repository import MongoDbUserRepository

injector = Injector()

injector.bind(MongoDbUserRepository, UserRepository)