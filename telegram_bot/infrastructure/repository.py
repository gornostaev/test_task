import datetime
from domain.repository import UserRepository
from domain.entities import User
from database import MongoDbConnection

class MongoDbUserRepository(UserRepository):
    def __init__(self, db: MongoDbConnection = MongoDbConnection(),
                 collection: str = "user_profile"):
        self._collection = db[collection]

    async def get_user(self, telegram_id: str) -> User | None:
        return await User(**self._collection.find_one({"telegram_id": telegram_id}))

    async def about_service(self):
        return await self._collection.aggregate([
            {
                "$group": {
                    "_id": None,
                    "total_users": {
                        "$sum": 1
                    },
                    "first_user_registered": {
                        "$first": "$created_at"
                    },
                    "last_user_seen": {
                        "$last": "$last_seen"
                    }
                }
            }
        ]).next()

    async def create_user(self, user) -> User | None:
        return await User(**self._collection.insert_one({
            "first_name": user.first_name,
            "last_name": user.last_name if user.last_name else "",
            "username": user.username if user.username else "",
            "telegram_id": user.id,
            "visits": 1,
            "created_at": datetime.datetime.now(tz=datetime.timezone.utc),
            "last_seen": datetime.datetime.now(tz=datetime.timezone.utc)
        }))

    async def update_user_stats(self, telegram_id: str) -> None:
        await self._collection.update_one(
            {"telegram_id": telegram_id},
            {"$inc": {"visits": 1},
             "$set": {
                 "last_seen": datetime.datetime.now(tz=datetime.timezone.utc)
             }}
        )


