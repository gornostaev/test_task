from unittest import mock, TestCase
import json
from main import Config, FigureFactory

class TestConfig(TestCase):
    mock_good_config_file_content = r"""
    {"data_root_folder": ".\\Data", "output_folder": ".\\Images"}
    """
    mock_bad_config_file_content = r"""
    ["data_root_folder": "/one", "output_folderrr": "/another"]
    """
    mock_circle_params = {"X_center": 45, "Y_center": 20, "Radius": 9}

    def test_config_file_missing(self):
        with mock.patch('builtins.open', side_effect=FileNotFoundError):
            self.assertRaises(FileNotFoundError, Config)

    def test_config_correct(self):
        with mock.patch('builtins.open',
                        new=mock.mock_open(read_data=self.mock_good_config_file_content),
                        create=True):
            try:
                Config()
            except Exception as e:
                self.fail(f"Config init failed with valid JSON input: {e}")

    def test_config_invalid(self):
        with mock.patch('builtins.open',
                        new=mock.mock_open(read_data=self.mock_bad_config_file_content),
                        create=True):
            self.assertRaises((json.decoder.JSONDecodeError, FileNotFoundError, KeyError), Config)

    def test_circle_draw_success(self):
        try:
            circle = FigureFactory.create_figure("Circle",self.mock_circle_params)
            circle.draw()
        except Exception as e:
            self.fail(f"Failed to draw circle: {e}")

    def test_circle_draw_failure(self):
        circle = FigureFactory.create_figure("Circle", {})
        self.assertRaises(KeyError, circle.draw)
