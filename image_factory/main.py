from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any
import json
import sys
import os
from pathlib import Path, PureWindowsPath

DEFAULT_LINE_WIDTH = 1
DEFAULT_TRANSPARENCY = "off"

class Image:
    def __init__(self,
                 width: int,
                 height: int,
                 name: str,
                 extension: str = "png"):
        self._w = width
        self._h = height
        self.name = name
        self.extension = extension
        self.figures: list[Figure] = []

    def render(self):
        elements = []
        for figure in  self.figures:
            elements.append(figure.draw())

        # img = image_library.render(image_params, image_elements)
        return "🌌🌅🌃🌠"

class Figure:
    def __init__(self,
                 line_width=DEFAULT_LINE_WIDTH,
                 transparency=DEFAULT_TRANSPARENCY,
                 **params):
        self.line_width = line_width
        self.transparency = transparency
        self.params = params

    def __repr__(self):
        return self.__class__.__name__ + ' ' + str(self.__dict__)

    @abstractmethod
    def draw(self) -> Any:
        """Method for drawing shapes"""

class FigureFactory:
    figures = {}

    @classmethod
    def add_figure(cls, new_figure_class: Figure):
        cls.figures[new_figure_class.__name__] = new_figure_class

    @classmethod
    def create_figure(cls, figure_type, params) -> Figure:
        if figure_type not in cls.figures:
            raise NotImplementedError(f'Figure {figure_type} is not implemented')
        return cls.figures[figure_type](**params)

class ImageFactory:
    def __init__(self,
                 file: str = sys.argv[1] if len(sys.argv) > 1 else 'config.json',
                 figure_factory: FigureFactory = FigureFactory()
                 ):
        self._figure_factory = figure_factory
        with open(file, 'r') as f:
            config_data = json.load(f)
            self._data_root_folder = str(Path(PureWindowsPath(config_data['data_root_folder'])))
            self._output_folder = str(Path(PureWindowsPath(config_data['output_folder'])))

    def _load_data(self) -> dict:
        '''
        json data example
        ps/that is what inside huge banks data going on, supplied as test task
        {
            "image_size": {
                "X_length" : 250,
                "Y_length" : 250
            },
            "figures": [
		        {
			        "Circle": {
				        "X_center": 45,
				        "Y_center": 20,
				        "Radius": 9
			        }
		        },
		        {
			        "Rectangle": {
                        "X_upper_left": 187,
                        "Y_upper_left": 218,
                        "X_lower_right": 230,
                        "Y_lower_right": 202
                    }
                }
            ]
        }
        '''
        data = os.listdir(self._data_root_folder)
        if not len(data):
            raise Exception('No data in data_root_folder')

        image_name, _ = os.path.splitext(data[0])
        image_data = {}
        with open(self._data_root_folder + '/' + data[0], 'r') as f:
            image_data = json.load(f)
        return {
            'name': image_name,
            'width': image_data.get('image_size', []).get('X_length', 0),
            'height': image_data.get('image_size', []).get('Y_length', 0),
            'figures': image_data.get('figures', []),
        }

    def create_image(self) -> Image:
        image_data = self._load_data()
        image = Image(
            name=image_data['name'],
            width=image_data['width'],
            height=image_data['height']
        )

        for f_data in image_data['figures']:
            figure_type, params = next(iter(f_data.items()))
            figure = self._figure_factory.create_figure(figure_type, params)

            image.figures.append(figure)

        return image

    def save_image(self, image: Image) -> bool:
        path = self._output_folder + '/' + image.name + '.' + image.extension
        with open(path, 'w') as f:
            f.write(image.render())
            return True
        return False

@FigureFactory.add_figure
class Circle(Figure):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
    def draw(self):
        try:
            x, y, r = self.params['X_center'], self.params['Y_center'], self.params['Radius']
        except KeyError as e:
            raise KeyError(f'Not all the values provided: {e}')
        # do drawings
        return True

@FigureFactory.add_figure
class Rectangle(Figure):
    def draw(self):
        pass

@FigureFactory.add_figure
class Square(Figure):
    def draw(self):
        pass

@FigureFactory.add_figure
class Triangle(Figure):
    def draw(self):
        pass

def draw_image(factory: ImageFactory = ImageFactory()):
    image = factory.create_image()
    factory.save_image(image)

